#!/bin/bash

echo "Bulding Nodejs server Image"
cd application-code/server
docker build . -t cicd-server

docker images
echo "Removing build image"
docker rmi cicd-server