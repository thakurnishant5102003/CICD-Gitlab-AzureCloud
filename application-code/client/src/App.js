import React from 'react'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import Home from './routes/Home'
import Create from './routes/Create'
import Read from './routes/Read'
import Update from './routes/Update'


const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/create' element={<Create />}/>
        <Route path='/read/:id' element={<Read />}/>
        <Route path='/edit/:id' element={<Update />}/>
      </Routes>
    </BrowserRouter>
  )
}

export default App