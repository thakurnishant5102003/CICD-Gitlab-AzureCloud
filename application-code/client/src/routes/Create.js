import React, { useState } from 'react'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

function Create() {
    
    const [record, setRecord] = useState({
        username: '',
        email: '',
        lucky_number: '' 
    });

    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();
        axios.post('http://localhost:8080/new_record', record)
        .then(res => {
            console.log(res);
            navigate('/')
        })
        .catch(err => console.log(err));
    }
  return (
    <div className='d-flex vh-100 bg-primary justify-content-center align-items-center'>
        <div className='w-50 bg-white rounded p-3'>
            <form onSubmit={handleSubmit}>
                <h2>Add User Info</h2>
                <div className='mb-2'>
                    <label htmlFor=''>Name</label>
                    <input type="text" placeholder='Enter Name' className='form-control'
                    onChange={e => setRecord({...record, username: e.target.value})}/>
                </div>
                <div className='mb-2'>
                    <label htmlFor=''>Email</label>
                    <input type="text" placeholder='Enter Email' className='form-control'
                    onChange={e => setRecord({...record, email: e.target.value})}/>
                </div>
                <div className='mb-2'>
                    <label htmlFor=''>Lucky Number</label>
                    <input type="number" placeholder='Enter Lucky Number' min="0" oninput="validity.valid||(value='');" className='form-control'
                    onChange={e => setRecord({...record, lucky_number: e.target.value})}/>
                </div>
                <button className='btn btn-success'>Submit</button>
            </form>
        </div>
    </div>
  )
}

export default Create
