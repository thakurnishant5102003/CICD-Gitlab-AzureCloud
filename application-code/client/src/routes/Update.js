import axios from 'axios';
import React, {useEffect, useState} from 'react'
import { useNavigate, useParams } from 'react-router-dom';

function Update() {

  const {id} = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    axios.get('http://localhost:8080/record/'+id)
    .then(res => {
        console.log(res);
        setRecord({...record, 
          username: res.data.username,
          email: res.data.email,
          lucky_number: res.data.lucky_number
        })
    })
    .catch(err => console.log(err));
  }, []);
  
  const [record, setRecord] = useState({
    username: '',
    email: '',
    lucky_number: '' 
  });

  const handleUpdate = (e) => {
    e.preventDefault();
    axios.put('http://localhost:8080/update_record/'+id, record)
    .then(res => {
      console.log(res);
      navigate('/')
    })
    .catch(err => console.log(err));
  }

  return (
    <div className='d-flex vh-100 bg-primary justify-content-center align-items-center'>
        <div className='w-50 bg-white rounded p-3'>
            <form onSubmit={handleUpdate}>
                <h2>Update User Info</h2>
                <div className='mb-2'>
                    <label htmlFor=''>Name</label>
                    <input type="text" placeholder='Enter Name' className='form-control' value={record.username}
                    onChange={e => setRecord({...record, username: e.target.value})}/>
                </div>
                <div className='mb-2'>
                    <label htmlFor=''>Email</label>
                    <input type="text" placeholder='Enter Email' className='form-control' value={record.email}
                    onChange={e => setRecord({...record, email: e.target.value})}/>
                </div>
                <div className='mb-2'>
                    <label htmlFor=''>Lucky Number</label>
                    <input type="number" placeholder='Enter Lucky Number' min="0" oninput="validity.valid||(value='');" 
                    value={record.lucky_number} className='form-control' onChange={e => setRecord({...record, lucky_number: e.target.value})}/>
                </div>
                <button className='btn btn-success'>Update</button>
            </form>
        </div>
    </div>
  )
}

export default Update
