import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom';

function Read() {
    const {id} = useParams();
    const [user, setUser] = useState([]);
    useEffect(() => {
        axios.get('http://localhost:8080/record/'+id)
        .then(res => {
            console.log(res);
            setUser(res.data);
        })
        .catch(err => console.log(err));
    }, []);
    return (
        <div className='d-flex vh-100 bg-primary justify-content-center align-items-center'>
            <div className='w-50 bg-white rounded p-3'>
                <h1>User Info</h1>
                <h3>ID: {user.id}</h3>
                <h3>Name: {user.username}</h3>
                <h3>Email: {user.email}</h3>
                <h3>Lucky Number: {user.lucky_number}</h3>
                <Link to="/" className='btn btn-primary me-2'>Back</Link>
                <Link to={`/edit/${user.id}`} className='btn btn-info'>Edit</Link>
            </div>
        </div>
  )
}

export default Read
