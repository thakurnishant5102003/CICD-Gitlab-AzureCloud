import React, {useEffect, useState} from 'react'
import axios from 'axios'
import { Link, useNavigate } from 'react-router-dom';

function Home() {
    
    const navigate = useNavigate();

    const [records, setRecords] = useState([])
    useEffect(() => {
        axios.get('http://localhost:8080/records')
        .then(res => setRecords(res.data))
        .catch(err => console.log(err));
    }, [])

    const handleDelete = (id) => {
        axios.delete('http://localhost:8080/delete_record/'+id)
        .then(res => {
            window.location.reload();
        })
        .catch(err => console.log(err));
    }

    return (
    <div className='d-flex vh-100 bg-primary justify-content-center align-items-center'>
      <div className='w-50 bg-white rounded p-3'>
        <h2>User Details</h2>
        <div className='d-flex justify-content-end'>
            <Link to="/create" className='btn btn-success'>Create +</Link>
        </div>
        <table className='table'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Lucky Number</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {records.map((user, index) => {
                    return <tr key={index}>
                        <td>{user.id}</td>
                        <td>{user.username}</td>
                        <td>{user.email}</td>
                        <td>{user.lucky_number}</td>
                        <td>
                            <Link to={`/read/${user.id}`} className='btn btn-sm btn-info'>Read</Link>
                            <Link to={`/edit/${user.id}`} className='btn btn-sm btn-primary mx-2'>Edit</Link>
                            <button onClick={ () => handleDelete(user.id) } className='btn btn-sm btn-danger'>Delete</button>
                        </td>
                    </tr>
                })}
            </tbody>
        </table>
      </div>
    </div>
  )
}

export default Home