import express from 'express';
import cors from 'cors'

import {getRecords, getRecord, createRecord, updateRecord, deleteRecord} from './database.js'

const app = express();

app.use(cors());

app.use(express.json());

// Get All Records on '/records'
app.get("/records", async (req, res) => {
    const records = await getRecords();
    res.send(records);
});

// Get Asked Record on '/record/3'
app.get("/record/:id", async (req, res) => {
    const id = req.params.id;
    const record = await getRecord(id);
    res.send(record);
});

app.post("/new_record", async (req, res) => {
    const {username, email, lucky_number} = req.body;
    const new_record = await createRecord(username, email, lucky_number);
    res.status(201).send(new_record);
});

app.put("/update_record/:id", async (req, res) => {
    const id = req.params.id;
    const {username, email, lucky_number} = req.body;
    const updated_record = await updateRecord(username, email, lucky_number, id);
    res.status(202).send(updated_record)
})

app.delete("/delete_record/:id", async (req, res) => {
    const id = req.params.id;
    const record = await deleteRecord(id);
    res.status(202).send(record);
})

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).json({
        message: 'Something is Broke in Database'
    });
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`Server running on PORT ${PORT}`));
