import mysql from 'mysql2'

import dotenv from 'dotenv'
dotenv.config();

const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWD,
    database: process.env.DB_NAME
}).promise();

export async function getRecords(){
    const [records] = await pool.query("SELECT * FROM UserInfo");
    return records;
}

export async function getRecord(id){
    const [record] = await pool.query(`
    SELECT * 
    FROM UserInfo
    WHERE id = ?
    `, [id]);
    return record[0];
}

export async function createRecord(username, email, lucky_number){
    const [result] = await pool.query(`
    INSERT INTO UserInfo (
        username, 
        email, 
        lucky_number
    )
    VALUES (?, ?, ?)
    `, [username, email, lucky_number]);
    const id = result.insertId
    return getRecord(id);
}

export async function updateRecord(username, email, lucky_number, id){
    const [result] = await pool.query(`
    UPDATE UserInfo SET 
        username=?, 
        email=?, 
        lucky_number=? 
    WHERE id = ?
    `, [username, email, lucky_number, id])
    return getRecord(id);
}

export async function deleteRecord(id){
    const _ = await pool.query(`
    DELETE 
    FROM UserInfo
    WHERE id = ?
    `, [id]);
}