-- Create Database for CICD
-- CREATE DATABASE cicd_database;

-- Select Database
USE cicd_database;

-- Create Userinfo
CREATE TABLE UserInfo (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(30) NOT NULL,
    email VARCHAR(30) NOT NULL,
    lucky_number INT,
    reading_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

-- Insert Test Data in Table UserInfo
INSERT INTO UserInfo (username, email, lucky_number) 
    VALUES ('First_User', 'firstuser01@gmail.com', '1');